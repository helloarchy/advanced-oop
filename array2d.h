#ifndef __ARRAY2D__
#define __ARRAY2D__

// ******************************************************************************************************************
// Includes - I only used two!
// ******************************************************************************************************************
#include <cstring>
#include <iostream>

// ******************************************************************************************************************
// Class Definition
// ******************************************************************************************************************
template<typename T>
class array2d {
private:
    // Member Variables
    size_t width;
    size_t height;
    T *data;

public:
    // Construction / Destruction
    /* Default Constructor */
    array2d<T>();

    /* Overload Constructor, allocate memory for given W and H */
    array2d<T>(size_t _width, size_t _height);

    /* Overload Constructor, assign default value to each data indices */
    array2d<T>(size_t _width, size_t _height, T default_value);

    /* Destructor */
    ~array2d();


    // Width / Height Accessors
    size_t const &get_width() const;

    size_t const &get_height() const;


    // 1D Index Operators
    T &operator[](const size_t &rhs) const;

    T &operator[](const size_t &rhs);


    // 2D Index Operators
    T &operator()(const int &rhs_w, const int &rhs_h) const;

    T &operator()(const int &rhs_w, const int &rhs_h);


    // Copy Constructor / Assignment Operator
    array2d(const array2d &other);

    array2d<T> &operator=(const array2d &other);


    // Move Constructor / Assignment Operator
    array2d(array2d &&old) noexcept;

    array2d<T> &operator=(array2d &&rhs) noexcept;


    // Scalar Assignment Operator
    array2d<T> &operator=(const size_t &rhs);


    // Negation Operator
    array2d<T> &operator-();


    // Element-wise Operators
    void check_sizes(const array2d<T> &rhs);

    array2d<T> &operator*(const array2d &rhs);

    array2d<T> &operator/(const array2d &rhs);

    array2d<T> &operator+(const array2d &rhs);

    array2d<T> &operator-(const array2d &rhs);


    // Self Element-wise Operators
    array2d<T> &operator*=(const array2d &rhs);

    array2d<T> &operator/=(const array2d &rhs);

    array2d<T> &operator+=(const array2d &rhs);

    array2d<T> &operator-=(const array2d &rhs);


    // Scalar Operators i.e. (x / 1)
    array2d<T> &operator*(const T &rhs);

    array2d<T> &operator/(const T &rhs);

    array2d<T> &operator+(const T &rhs);

    array2d<T> &operator-(const T &rhs);


    // Self Scalar Operators i.e. (x /= 1) equiv to (x = x / 1)
    array2d<T> &operator*=(const T &rhs);

    array2d<T> &operator/=(const T &rhs);

    array2d<T> &operator+=(const T &rhs);

    array2d<T> &operator-=(const T &rhs);


    // Friend Scalar Operators i.e. (1 / x) as opposed to (x / 1)
    template<typename S>
    friend array2d<S> operator*(const S &lhs, const array2d<S> &rhs);

    template<typename S>
    friend array2d<S> operator/(const S &lhs, const array2d<S> &rhs);

    template<typename S>
    friend array2d<S> operator+(const S &lhs, const array2d<S> &rhs);

    template<typename S>
    friend array2d<S> operator-(const S &lhs, const array2d<S> &rhs);


    // Cast Operator
    template<typename R>
    explicit operator array2d<R>() const;


    // std::ostream << Operator
    template<typename S>
    friend std::ostream &operator<<(std::ostream &os, const array2d<S> &array2D);
};



// ******************************************************************************************************************
// Construction / Destruction
// ******************************************************************************************************************
/* Default Constructor */
template<typename T>
array2d<T>::array2d() : width(0), height(0), data(nullptr) {}


/* Overload Constructor, allocate memory for given W and H */
template<typename T>
array2d<T>::array2d(size_t _width, size_t _height) {
    width = _width;
    height = _height;
    /* Allocate memory */
    size_t size = (width * height);
    if (size > 0) data = new T[size];
}


/* Overload Constructor, assign default value to each data indices */
template<typename T>
array2d<T>::array2d(size_t _width, size_t _height, T _default_value) {
    width = _width;
    height = _height;
    /* Allocate memory */
    size_t size = (width * height);
    if (size > 0) {
        data = new T[size];
        /* Assign values to default */
        for (size_t i = 0; i < size; i++) {
            data[i] = _default_value;
        }
    }
}


/* Destructor */
template<typename T>
array2d<T>::~array2d() {
    if (data != nullptr) {
        delete[] data;
        data = nullptr;
    }
}


// ******************************************************************************************************************
// Width / Height Accessors
// ******************************************************************************************************************
template<typename T>
size_t const &array2d<T>::get_width() const {
    return width;
}


template<typename T>
size_t const &array2d<T>::get_height() const {
    return height;
}


// ******************************************************************************************************************
// 1D Index Operators
// ******************************************************************************************************************
template<typename T>
T &array2d<T>::operator[](const size_t &rhs) const {
    return data[rhs];
}


template<typename T>
T &array2d<T>::operator[](const size_t &rhs) {
    return data[rhs];
}


// ******************************************************************************************************************
// 2D Index Operators
// ******************************************************************************************************************
template<typename T>
T &array2d<T>::operator()(const int &rhs_w, const int &rhs_h) const {
    /* Offset 1D index to mimic 2D array behaviour */
    return data[(rhs_h * width) + rhs_w];
}


template<typename T>
T &array2d<T>::operator()(const int &rhs_w, const int &rhs_h) {
    /* Offset 1D index to mimic 2D array behaviour */
    return data[(rhs_h * width) + rhs_w];
}


// ******************************************************************************************************************
// Copy Constructor / Assignment Operator
// ******************************************************************************************************************
/* Copy Constructor */
template<typename T>
array2d<T>::array2d(const array2d &other) {
    width = other.width;
    height = other.height;
    /* Allocate memory and copy data */
    size_t size = (width * height);
    if (size > 0) {
        data = new T[size];
        memcpy(data, other.data, size * sizeof(T));
    }
}


/* Copy Assignment */
template<typename T>
array2d<T> &array2d<T>::operator=(const array2d &other) {
    if (&other == this) return *this;
    /* Clear data */
    if (data != nullptr) {
        delete[] data;
        data = nullptr;
    }
    width = other.width;
    height = other.height;
    /* Allocate memory and copy data */
    size_t size = (width * height);
    if (size > 0) {
        data = new T[size];
        memcpy(data, other.data, size * sizeof(T));
    }
    return *this;
}


// ******************************************************************************************************************
// Move Constructor / Assignment Operator
// ******************************************************************************************************************
template<typename T>
array2d<T>::array2d(array2d &&old) noexcept {
    /* Allocate memory and move over data */
    width = old.width;
    height = old.height;
    size_t size = (width * height);
    if (size > 0) {
        data = new T[size];
        memmove(data, old.data, size * sizeof(T));
    }
    /* Remove old values */
    old.width = 0;
    old.height = 0;
    delete[] old.data;
    old.data = nullptr;
}


template<typename T>
array2d<T> &array2d<T>::operator=(array2d &&rhs) noexcept {
    if (&rhs == this) return *this;
    /* Allocate memory and move over data */
    width = rhs.width;
    height = rhs.height;
    size_t size = (width * height);
    if (size > 0) {
        data = new T[size];
        memmove(data, rhs.data, size * sizeof(T));
    }
    /* Remove old values */
    rhs.width = 0;
    rhs.height = 0;
    delete[] rhs.data;
    rhs.data = nullptr;

    return *this;
}


// ******************************************************************************************************************
// Scalar Assignment Operator
// ******************************************************************************************************************
template<typename T>
array2d<T> &array2d<T>::operator=(const size_t &rhs) {
    /* Assign value to all indices */
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        data[i] = rhs;
    }
    return *this;
}


// ******************************************************************************************************************
// Negation Operator
// ******************************************************************************************************************
template<typename T>
array2d<T> &array2d<T>::operator-() {
    /* Negate all values in new instance */
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        data[i] = -data[i];
    }
    return *this;
}


// ******************************************************************************************************************
// Element-wise Operators
// ******************************************************************************************************************
template<typename T>
void array2d<T>::check_sizes(const array2d<T> &rhs) {
    /* Throw exception if sizes do not match */
    if ((width != rhs.width) && (height != rhs.height)) {
        throw std::length_error("Array sizes do not match!");
    }
}


template<typename T>
array2d<T> &array2d<T>::operator*(const array2d &rhs) {
    check_sizes(rhs);
    /* Multiply all values in new instance with those from rhs */
    auto *result = new array2d(width, height);
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        result->data[i] = data[i] * rhs.data[i];
    }
    return *result;
}


template<typename T>
array2d<T> &array2d<T>::operator/(const array2d &rhs) {
    check_sizes(rhs);
    /* Divide all values in new instance with those from rhs */
    auto *result = new array2d(width, height);
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        result->data[i] = data[i] / rhs.data[i];
    }
    return *result;
}


template<typename T>
array2d<T> &array2d<T>::operator+(const array2d &rhs) {
    check_sizes(rhs);
    /* Add all values in new instance with those from rhs */
    auto *result = new array2d(width, height);
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        result->data[i] = data[i] + rhs.data[i];
    }
    return *result;
}


template<typename T>
array2d<T> &array2d<T>::operator-(const array2d &rhs) {
    check_sizes(rhs);
    /* Subtract all values in new instance with those from rhs */
    auto *result = new array2d(width, height);
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        result->data[i] = data[i] - rhs.data[i];
    }
    return *result;
}


// ******************************************************************************************************************
// Self Element-wise Operators
// ******************************************************************************************************************
template<typename T>
array2d<T> &array2d<T>::operator*=(const array2d &rhs) {
    check_sizes(rhs);
    /* Multiply all values with those from rhs */
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        data[i] *= rhs.data[i];
    }
    return *this;
}


template<typename T>
array2d<T> &array2d<T>::operator/=(const array2d &rhs) {
    check_sizes(rhs);
    /* Divide all values with those from rhs */
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        data[i] /= rhs.data[i];
    }
    return *this;
}


template<typename T>
array2d<T> &array2d<T>::operator+=(const array2d &rhs) {
    check_sizes(rhs);
    /* Add all values with those from rhs */
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        data[i] += rhs.data[i];
    }
    return *this;
}


template<typename T>
array2d<T> &array2d<T>::operator-=(const array2d &rhs) {
    check_sizes(rhs);
    /* Minus all values with those from rhs */
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        data[i] -= rhs.data[i];
    }
    return *this;
}


// ******************************************************************************************************************
// Scalar Operators
// ******************************************************************************************************************
template<typename T>
array2d<T> &array2d<T>::operator*(const T &rhs) {
    /* Multiply all values by rhs */
    auto *result = new array2d(width, height);
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        result->data[i] = data[i] * rhs;
    }
    return *result;
}


template<typename T>
array2d<T> &array2d<T>::operator/(const T &rhs) {
    /* Divide all values by rhs */
    auto *result = new array2d(width, height);
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        result->data[i] = data[i] / rhs;
    }
    return *result;
}


template<typename T>
array2d<T> &array2d<T>::operator+(const T &rhs) {
    /* Add all values with rhs */
    auto *result = new array2d(width, height);
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        result->data[i] = data[i] + rhs;
    }
    return *result;
}


template<typename T>
array2d<T> &array2d<T>::operator-(const T &rhs) {
    /* Minus all values with rhs */
    auto *result = new array2d(width, height);
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        result->data[i] = data[i] - rhs;
    }
    return *result;
}


// ******************************************************************************************************************
// Self Scalar Operators
// ******************************************************************************************************************
template<typename T>
array2d<T> &array2d<T>::operator*=(const T &rhs) {
    /* Multiply all values by rhs */
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        data[i] *= rhs;
    }
    return *this;
}


template<typename T>
array2d<T> &array2d<T>::operator/=(const T &rhs) {
    /* Divide all values by rhs */
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        data[i] /= rhs;
    }
    return *this;
}


template<typename T>
array2d<T> &array2d<T>::operator+=(const T &rhs) {
    /* Add all values with rhs */
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        data[i] += rhs;
    }
    return *this;
}


template<typename T>
array2d<T> &array2d<T>::operator-=(const T &rhs) {
    /* Minus all values with rhs */
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        data[i] -= rhs;
    }
    return *this;
}


// ******************************************************************************************************************
// Friend Scalar Operators
// ******************************************************************************************************************
template<typename T>
array2d<T> operator*(const T &lhs, const array2d<T> &rhs) {
    /* Multiply all values by rhs */
    auto *result = new array2d<T>(rhs.width, rhs.height);
    size_t size = rhs.width * rhs.height;
    for (size_t i = 0; i < size; i++) {
        result->data[i] = lhs * rhs.data[i];
    }
    return *result;
}


template<typename T>
array2d<T> operator/(const T &lhs, const array2d<T> &rhs) {
    /* Divide all values by rhs */
    auto *result = new array2d<T>(rhs.width, rhs.height);
    size_t size = rhs.width * rhs.height;
    for (size_t i = 0; i < size; i++) {
        result->data[i] = lhs / rhs.data[i];
    }
    return *result;
}


template<typename T>
array2d<T> operator+(const T &lhs, const array2d<T> &rhs) {
    /* Add all values with rhs */
    auto *result = new array2d<T>(rhs.width, rhs.height);
    size_t size = rhs.width * rhs.height;
    for (size_t i = 0; i < size; i++) {
        result->data[i] = lhs + rhs.data[i];
    }
    return *result;
}


template<typename T>
array2d<T> operator-(const T &lhs, const array2d<T> &rhs) {
    /* Minus all values with rhs */
    auto *result = new array2d<T>(rhs.width, rhs.height);
    size_t size = rhs.height * rhs.height;
    for (size_t i = 0; i < size; i++) {
        result->data[i] = lhs - rhs.data[i];
    }
    return *result;
}


// ******************************************************************************************************************
// Cast Operator
// ******************************************************************************************************************
template<typename T>
template<typename R>
array2d<T>::operator array2d<R>() const {
    array2d<R> result(width, height);
    size_t size = width * height;
    for (size_t i = 0; i < size; i++) {
        result[i] = R(data[i]);
    }
    return result;
}


// ******************************************************************************************************************
// std::ostream << Operator
// ******************************************************************************************************************
template<typename T>
std::ostream &operator<<(std::ostream &os, const array2d<T> &array2D) {
    os << "[";
    /* Print rows */
    for (size_t i = 0; i < array2D.height; i++) {
        os << (i == 0 ? " [ " : "  [ ");
        /* Print columns */
        for (size_t j = 0; j < array2D.width; j++) {
            os << (array2D(j, i));
            if (j < (array2D.width - 1)) os << ", ";
        }
        os << " ]";
        if (i < (array2D.height - 1)) os << ",\n";
    }
    os << " ]";
    return os;
}


#endif